<?php Namespace WordPress\Plugin\GalleryManager;

abstract class WPML {

  public static function init(){
    add_Filter('gettext_with_context', [static::class, 'filterGettextWithContext'], 1, 4);
  }

  public static function isWPMLActive(){
    return defined('ICL_SITEPRESS_VERSION');
  }

  public static function filterGettextWithContext($translation, $text, $context, $domain){
    # If you are using WPML the post type slug MUST NOT be translated! You can translate your slug in WPML directly
    if (static::isWPMLActive() && $context == 'URL slug' && $domain == I18n::getTextDomain())
      return $text;
    else
      return $translation;
  }

}

WPML::init();
