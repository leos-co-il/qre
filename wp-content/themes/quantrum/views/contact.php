<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
$whatsapp = opt('whatsapp');
$map = opt('map_image');
?>

<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'top_block', [
		'title' => get_the_title(),
		'img' => isset($fields['top_img']) && $fields['top_img'] ? $fields['top_img']['url'] : (has_post_thumbnail() ? postThumb() : ''),
	]); ?>
	<div class="contact-page-body">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-lg-11 col-12">
					<div class="row justify-content-center">
						<?php if ($tel) : ?>
							<div class="col-lg-3 col-sm-6 col-11 contact-item contact-item-link wow zoomIn"
								 data-wow-delay="0.2s">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-whatsapp.png">
								</div>
								<a href="tel:<?= $tel; ?>" class="contact-info">
									<?= $tel; ?>
								</a>
							</div>
						<?php endif;
						if ($whatsapp) : ?>
							<div class="contact-item col-lg-3 col-sm-6 col-11 wow zoomIn" data-wow-delay="0.4s">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-whatsapp.png">
								</div>
								<a class="contact-info" href="https://api.whatsapp.com/send?phone=<?= $whatsapp; ?>">
									<?= $whatsapp; ?>
								</a>
							</div>
						<?php endif;
						if ($mail) : ?>
							<div class="contact-item col-lg-3 col-sm-6 col-11 contact-item-link wow zoomIn"
								 data-wow-delay="0.6s">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-mail.png">
								</div>
								<a href="mailto:<?= $mail; ?>" class="contact-info">
									<?= $mail; ?>
								</a>
							</div>
						<?php endif; ?>
						<?php if ($address) : ?>
							<a href="https://www.waze.com/ul?q=<?= $address; ?>"
							   class="contact-item-link contact-item col-lg-3 col-sm-6 col-11 wow zoomIn" data-wow-delay="0.8s">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-geo.png">
								</div>
								<p class="contact-info">
									<?= $address; ?>
								</p>
							</a>
						<?php endif; ?>
					</div>
					<div class="contact-form-wrap mb-5 wow zoomIn dark-back" data-wow-delay="0.4s">
						<?php if ($fields['contact_form_title'] && $fields['contact_form_subtitle']) : ?>
							<div class="row contact-form-text justify-content-center mb-3">
								<h2 class="col-auto contact-form-title"><?= $fields['contact_form_title']; ?></h2>
								<h2 class="col-auto contact-form-subtitle"><?= $fields['contact_form_subtitle']; ?></h2>
							</div>
						<?php endif;
						getForm('11'); ?>
					</div>
					<?php if ($map) : ?>
						<div class="row">
							<div class="col-12">
								<a class="map-image" href="<?= $map['url']; ?>" data-lightbox="map">
									<img src="<?= $map['url']; ?>" alt="map">
								</a>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
