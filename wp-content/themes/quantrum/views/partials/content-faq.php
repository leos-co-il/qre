<?php if (isset($args['faq'])) : ?>
	<section class="faq">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-9 col-lg-10 col-12">
					<div class="row justify-content-center">
						<div class="col-12 d-flex flex-column justify-content-center align-items-center">
							<?php if (isset($args['block_title'])) : ?>
								<h2 class="base-title text-center">
									<?= $args['block_title']; ?>
								</h2>
							<?php endif; ?>
						</div>
					</div>
					<div class="row justify-content-center">
						<div class="col-12">
							<div id="accordion">
								<?php foreach ($args['faq'] as $num => $item) : ?>
									<div class="card question-card wow fadeInUp" data-wow-delay="0.<?= $num + $i = 1; ?>s" <?php $i++; ?>>
										<div class="question-header" id="heading_<?= $num; ?>">
											<button class="btn question-title" data-toggle="collapse"
													data-target="#faqChild<?= $num; ?>"
													aria-expanded="false" aria-controls="collapseOne">
										<span class="faq-icon">
											<img src="<?= ICONS ?>faq.png">
										</span>
												<span><?= $item['faq_question']; ?></span>
												<span class="faq-sign faq-plus">+</span>
												<span class="faq-sign faq-minus">-</span>
											</button>
											<div id="faqChild<?= $num; ?>" class="collapse faq-item"
												 aria-labelledby="heading_<?= $num; ?>" data-parent="#accordion">
												<div class="answer-body base-output">
													<?= $item['faq_answer']; ?>
												</div>
											</div>
										</div>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
