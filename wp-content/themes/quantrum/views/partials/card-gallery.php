<?php if (isset($args['gallery']) && $args['gallery']) :
$m = (isset($args['num']) && $args['num']) ? $args['num'] : '';
?>
	<div class="container-projects-home mb-1 gallery-container gallery-pro-<?= $m; ?> more-card" data-id="<?= isset($args['num']) && $args['num'] ? $args['num'] : '' ?>">
		<?php foreach ($args['gallery'] as $num => $project) : ?>
			<div class="col-project-home col-project-<?= $num + 1; ?>">
				<a class="gallery-overlay" href="<?= $project['url']; ?>" data-lightbox="gallery-<?= $m; ?>">+</a>
				<img src="<?= $project['url']; ?>" alt="project-img">
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>
