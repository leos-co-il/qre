<?php if(isset($args['member']) && $args['member']) : ?>
		<div class="post-card more-card worker-card">
			<div class="flex-grow-1 d-flex flex-column w-100">
				<div class="grad-back">
					<div class="inside-image">
						<div class="bordered-image bordered-image-post">
							<?php if (isset($args['member']['team_item']) && $args['member']['team_item']) : ?>
							<?php endif; ?>
							<img src="<?= $args['member']['team_item']['url']; ?>">
						</div>
					</div>
				</div>
				<div class="post-item-content">
					<h3 class="post-item-title text-center mb-2"><?= (isset($args['member']['team_name']) && $args['member']['team_name']) ?
							$args['member']['team_name'] : ''; ?></h3>
					<h3 class="post-item-subtitle"><?= (isset($args['member']['team_position']) && $args['member']['team_position']) ?
							$args['member']['team_position'] : ''; ?></h3>
					<p class="post-preview text-center">
						<?= (isset($args['member']['team_description']) && $args['member']['team_description']) ?
							$args['member']['team_description'] : ''; ?>
					</p>
				</div>
			</div>
		</div>
<?php endif; ?>
