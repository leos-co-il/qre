<?php if ($benefits = opt('benefits')) : ?>
	<section class="dark-back benefits-block">
		<div class="container">
			<div class="row justify-content-center">
				<?php if ($title = opt('benefits_block_title')) : ?>
					<div class="col-12">
						<h2 class="base-title-white mb-5"><?= $title; ?></h2>
					</div>
				<?php endif; ?>
				<div class="col-12">
					<?php foreach (array_chunk($benefits, 2) as $row) : ?>
						<div class="row justify-content-center align-items-stretch why-item-row-wrap">
							<?php foreach ($row as $item) : ?>
								<div class="col-lg-6 col-12 why-item-col">
									<div class="why-item-row">
										<div class="why-item-content">
											<?php if ($item['icon']) : ?>
												<div class="why-item-icon">
													<img src="<?= $item['icon']['url']; ?>">
												</div>
											<?php endif; ?>
											<h3 class="why-item-title"><?= $item['title']; ?></h3>
											<p class="why-item-text"><?= $item['desc']; ?></p>
										</div>
										<div class="why-item-image" <?php if ($item['img']) : ?>
											style="background-image: url('<?= $item['img']['url']; ?>')"
										<?php endif; ?>></div>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
