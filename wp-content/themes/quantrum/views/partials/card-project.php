<?php if(isset($args['post']) && $args['post']) :
	$link = get_the_permalink($args['post']); ?>
	<div class="col-12 project-col">
		<div class="row project-row more-card" data-id="<?= $args['post']->ID; ?>">
			<a class="col-sm-6 col-12 project-img-col" href="<?= $link; ?>">
				<?php if (has_post_thumbnail($args['post'])) : ?>
					<img src="<?= postThumb($args['post']); ?>" alt="project-image">
				<?php endif; ?>
			</a>
			<div class="col-sm-6 col-12 project-title-col">
				<a href="<?= $link; ?>" class="project-item-title"><?= $args['post']->post_title; ?></a>
			</div>
		</div>
	</div>
<?php endif; ?>
