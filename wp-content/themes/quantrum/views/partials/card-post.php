<?php if(isset($args['post']) && $args['post']) :
    $link = get_the_permalink($args['post']); ?>
<div class="col-lg-4 col-sm-6 col-12 col-post">
	<div class="post-card more-card" data-id="<?= $args['post']->ID; ?>">
		<div class="flex-grow-1 d-flex flex-column w-100">
			<div class="grad-back">
				<div class="inside-image">
					<a class="bordered-image bordered-image-post" href="<?= $link; ?>">
						<?php if (has_post_thumbnail($args['post'])) : ?>
						<?php endif; ?>
						<img src="<?= postThumb($args['post']); ?>">
					</a>
				</div>
			</div>
			<div class="post-item-content">
				<h3 class="post-item-title"><?= $args['post']->post_title; ?></h3>
				<p class="post-preview mb-3">
					<?= text_preview($args['post']->post_content, 20); ?>
				</p>
			</div>
		</div>
		<a href="<?php the_permalink($args['post']); ?>" class="post-link align-self-end">
			המשך קריאה
		</a>
	</div>
</div>
<?php endif; ?>
