<section class="top-block" <?php if (isset($args['img']) && $args['img']) : ?>
style="background-image: url('<?= $args['img']; ?>')"
	<?php endif; ?>>
	<span class="top-overlay"></span>
	<?php if (isset($args['title']) && $args['title']) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<h1 class="top-block-title">
						<?= $args['title']; ?>
					</h1>
				</div>
			</div>
		</div>
	<?php endif; ?>
</section>
<div class="container container-breadcrumbs">
	<div class="row">
		<div class="col-auto">
			<?php site_breadcrumbs(); ?>
		</div>
	</div>
</div>
