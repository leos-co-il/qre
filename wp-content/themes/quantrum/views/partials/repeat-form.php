<?php
$f_title = (isset($args['title']) && $args['title']) ? $args['title'] : opt('base_form_title');
$id = (isset($args['id']) && $args['id']) ? $args['id'] : '13';
?>
<section class="repeat-form-block">
	<div class="container">
		<div class="row">
			<?php if ($f_title) : ?>
				<div class="col-12">
					<h2 class="form-title">
						<?= $f_title; ?>
					</h2>
				</div>
			<?php endif; ?>
			<div class="col-12">
				<?php getForm($id); ?>
			</div>
		</div>
	</div>
</section>
