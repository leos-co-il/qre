<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();
$logo_main = opt('logo_white');
if ($fields['main_slider']) : ?>
	<section class="main-slider-block">
		<div class="main-slider" dir="rtl">
			<?php foreach ($fields['main_slider'] as $slide) : ?>
				<div class="main-slide-item" <?php if ($slide['img']) : ?>
					 style="background-image: url('<?= $slide['img']['url']; ?>')" <?php endif; ?>>
					<span class="main-slide-overlay"></span>
					<div class="container">
						<div class="row">
							<div class="col-12">
								<div class="main-slide-content">
									<?php if ($logo_main) : ?>
										<div class="logo mt-5 mb-4">
											<img src="<?= $logo_main['url']; ?>" alt="logo-quantrum">
										</div>
									<?php endif;
									if ($slide['main_title']) : ?>
										<h2 class="h-main-title"><?= $slide['main_title']; ?></h2>
									<?php endif;
									if ($slide['main_subtitle']) : ?>
										<h3 class="h-main-subtitle"><?= $slide['main_subtitle']; ?></h3>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</section>
<?php endif;
get_template_part('views/partials/repeat', 'form', [
		'title' => $fields['h_form_title'],
		'id' => '12',
]);
if ($fields['h_about_text'] || $fields['h_about_img']) : ?>
<article class="article-page-body">
	<div class="container">
		<div class="row justify-content-between align-items-center">
			<?php if ($fields['h_about_text']) : ?>
				<div class="<?= $fields['h_about_img'] ? 'col-xl-5 col-lg-6 col-12' : 'col-12'; ?>">
					<div class="base-output about-output">
						<?= $fields['h_about_text']; ?>
					</div>
					<?php if (['h_about_link']) : ?>
						<div class="row justify-content-end mt-4">
							<div class="col-auto">
								<a href="<?= $fields['h_about_link']['url'];?>" class="about-link">
									<?= (isset($fields['h_about_link']['title']) && $fields['h_about_link']['title'])
											? $fields['h_about_link']['title'] : esc_html__('קרא עוד עלינו', 'leos');
									?>
								</a>
							</div>
						</div>
					<?php endif; ?>
				</div>
			<?php endif;
			if ($fields['h_about_img']) : ?>
				<div class="<?= $fields['h_about_text'] ? 'col-xl-5 col-lg-6 col-12' : 'col-12'; ?> about-img-col">
					<div class="grad-back">
						<div class="inside-image">
							<div class="bordered-image">
								<img src="<?= $fields['h_about_img']['url']; ?>" alt="image">
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php endif;
get_template_part('views/partials/repeat', 'benefits');
if ($fields['h_pro_img'] && $fields['h_projects']) : ?>
	<section class="projects-home" <?php if ($fields['h_pro_img_cat']) : ?>
		style="background-image: url('<?= $fields['h_pro_img_cat']['url']; ?>')"
	<?php endif; ?>>
		<div class="container">
			<div class="row">
				<?php if ($fields['h_pro_title']) : ?>
					<div class="col-12">
						<h2 class="base-title text-center">
							<?= $fields['h_pro_title']; ?>
						</h2>
					</div>
				<?php endif; ?>
				<div class="col-12 position-relative">
					<div class="row justify-content-center our-projects">
						<?php foreach ($fields['h_projects'] as $pro) : ?>
							<div class="col-md-6 col-12 col-pro-home">
								<a href="<?= get_the_permalink($pro); ?>" class="project-circle">
									<h4 class="post-home-title">
										<?= $pro->post_title; ?>
									</h4>
									<p class="base-text text-center">
										<?= text_preview($pro->post_content, '15'); ?>
									</p>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
					<img src="<?= $fields['h_pro_img']['url']; ?>" alt="our-projects" class="w-100">
				</div>
			</div>
		</div>
	</section>
<?php endif;
get_template_part('views/partials/repeat', 'form'); ?>


<div class="container">
    <div class="row">
        <div class="col-12">
        </div>
    </div>
</div>


<?php if ($fields['h_posts']) : ?>
	<section class="posts-home-block">
		<div class="container">
			<div class="row justify-content-center">
				<?php if ($fields['h_posts_title']) : ?>
					<div class="col">
						<h2 class="base-title">
							<?= $fields['h_posts_title']; ?>
						</h2>
					</div>
				<?php endif; ?>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($fields['h_posts'] as $post) {
					get_template_part('views/partials/card', 'post', [
							'post' => $post
					]); } ?>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['single_slider_seo']) : ?>
	<div class="reverse-slider">
		<?php get_template_part('views/partials/content', 'slider', [
				'content' => $fields['single_slider_seo'],
				'img' => $fields['slider_img'],
		]); ?>
	</div>
<?php endif;
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq',
			[
					'block_title' => $fields['faq_title'],
					'faq' => $fields['faq_item'],
			]);
}
get_footer(); ?>
