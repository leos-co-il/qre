<?php
/*
Template Name: אודות
*/

get_header();
$fields = get_fields();
?>
<article class="page-body blog-body">
	<?php get_template_part('views/partials/repeat', 'top_block', [
			'title' => get_the_title(),
			'img' => isset($fields['top_img']) && $fields['top_img'] ? $fields['top_img']['url'] : (has_post_thumbnail() ? postThumb() : ''),
	]); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-9 col-sm-10 col-12">
				<div class="base-output text-center mb-4">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($fields['about_team_item']) : ?>
			<div class="row align-items-stretch justify-content-center">
				<?php foreach ($fields['about_team_item'] as $u => $post) : ?>
					<div class="col-lg-4 col-sm-6 col-12 col-post wow fadeInUpBig" data-wow-delay="0.<?= $u + 1;?>s">
						<?php get_template_part('views/partials/card', 'worker',
							[
									'member' => $post,
							]); ?>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
</article>
<div class="golden-form">
	<?php get_template_part('views/partials/repeat', 'form'); ?>
</div>
<?php get_template_part('views/partials/repeat', 'benefits');
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq',
			[
					'block_title' => $fields['faq_title'],
					'faq' => $fields['faq_item'],
			]);
}
get_footer(); ?>

