<?php
/*
Template Name: פרויקטים
*/
get_header();
$fields = get_fields();
$posts = new WP_Query([
	'posts_per_page' => 4,
	'post_type' => 'project',
	'suppress_filters' => false
]);
$published_posts = new WP_Query([
	'posts_per_page' => -1,
	'post_type' => 'project',
	'suppress_filters' => false,
]);
$terms = get_terms([
		'taxonomy'      => 'project_cat',
		'hide_empty'    => false,
		'parent'        => 0
]);
?>
<article class="page-body blog-body">
	<?php get_template_part('views/partials/repeat', 'top_block', [
		'title' => get_the_title(),
		'img' => isset($fields['top_img']) && $fields['top_img'] ? $fields['top_img']['url'] : (has_post_thumbnail() ? postThumb() : ''),
	]); ?>
	<div class="container mb-5">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($terms) : ?>
			<div class="row align-items-stretch justify-content-center mt-4 mb-3">
				<?php foreach ($terms as $term) : ?>
					<a href="<?= get_term_link($term); ?>" class="col-auto term-link-top">
						<?= $term->name; ?>
					</a>
				<?php endforeach; ?>
			</div>
		<?php endif; if ($posts->have_posts()) : ?>
			<div class="row align-items-stretch put-here-posts justify-content-center">
				<?php foreach ($posts->posts as $post) {
					get_template_part('views/partials/card', 'project',
						[
							'post' => $post,
						]);
				} ?>
			</div>
		<?php endif;
		if ($published_posts->have_posts() && (($num = count($published_posts->posts)) > 4)) : ?>
			<div class="row justify-content-center mt-5">
				<div class="col-auto">
					<div class="more-link regular-link load-more-posts" data-type="project" data-count="<?= $num; ?>">
						<?= esc_html__('טען עוד..', 'leos'); ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php get_footer(); ?>

