<?php
the_post();
get_header();
$fields = get_fields();
$icons_data = [
	['name' => esc_html__('סוג הנכס', 'leos'), 'data' => $fields['type'], 'icon' => ICONS.'project-type.png'],
	['name' => esc_html__('שותף מקצועי', 'leos'), 'data' => $fields['partner'], 'icon' => ICONS.'project-relations.png'],
	['name' => esc_html__('תאריך גיוס', 'leos'), 'data' => $fields['pro_date'], 'icon' => ICONS.'project-date.png'],
	['name' => esc_html__('סטטוס ההשקעה', 'leos'), 'data' => $fields['pro_status'], 'icon' => ICONS.'project-status.png'],
	['name' => esc_html__('השקעה מינימלית', 'leos'), 'data' => $fields['pro_invest'], 'icon' => ICONS.'project-price.png'],
];
$f_title = opt('post_form_title');
$f_subtitle = opt('post_form_subtitle');
?>
<article class="page-body blog-body">
	<div class="project-content-block">
		<div class="container breadcrumbs-white">
			<div class="row">
				<div class="col-auto">
					<?php site_breadcrumbs(); ?>
				</div>
			</div>
		</div>
		<div class="project-content-img" <?php if (has_post_thumbnail()) : ?>
			style="background-image: url('<?= postThumb(); ?>')"
		<?php endif; ?>></div>
		<div class="project-main-content">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-auto">
						<h1 class="top-block-title">
							<?php the_title(); ?>
						</h1>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-xl-8 col-lg-9 col-sm-10 col-12">
						<div class="base-output text-center mb-4">
							<h2 class="main-info-title"><?= esc_html__('מידע כללי', 'leos'); ?></h2>
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if ($icons_data) : ?>
		<div class="dark-back dark-back-data">
			<div class="container">
				<div class="row justify-content-center">
					<?php foreach ($icons_data as $x => $data) : ?>
						<div class="col-xxl col-lg-4 col-sm-6 col-12 wow fadeInUp" data-wow-delay="0.<?= $x + 1; ?>s">
							<div class="item-col">
								<div class="icon-item-wrap">
									<img src="<?= $data['icon']; ?>" alt="icon">
								</div>
								<h3 class="item-data-title"><?= $data['name']; ?></h3>
								<h4 class="item-data-text"><?= $data['data']; ?></h4>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php $steps_title = $fields['steps_title'] ? $fields['steps_title'] : opt('steps_title');
if ($steps = (isset($args['pro_steps']) && $args['pro_steps']) ? $args['pro_steps'] : opt('pro_steps')) : ?>
	<section class="steps-block">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto d-flex justify-content-center">
					<h2 class="base-title same-title-underline text-center mb-5">
						<?= $steps_title ? $steps_title : esc_html__('שלבים בהשקעה', 'leos');?>
					</h2>
				</div>
			</div>
			<div class="row">
				<?php foreach ($steps as $step) : ?>
					<div class="col-lg-4 col-sm-6 col-12 steps-column">
						<div class="step-item">
							<div class="step-icon-wrap">
								<?php if ($step['icon']) : ?>
									<img src="<?= $step['icon']['url']; ?>" alt="step-item">
								<?php endif; ?>
							</div>
							<div class="step-content-wrap">
								<h3 class="step-item-title"><?= $step['title']; ?></h3>
								<p class="step-item-text"><?= $step['description']; ?></p>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
<?php endif; ?>
	<section class="block-form-project">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<h2 class="base-title same-title-underline text-center mb-5">
						<?= esc_html__('מפרט הפרויקט', 'leos'); ?>
					</h2>
				</div>
			</div>
		</div>
		<div class="project-form-row">
			<div class="container">
				<div class="row align-items-stretch">
					<?php if ($fields['project_tabs']) : ?>
						<div class="col-xl-8 col-lg-7 col-12">
							<div class="tabs-content-pro">
								<ul class="nav nav-tabs" id="myTab" role="tablist">
									<?php foreach ($fields['project_tabs'] as $y => $item) : ?>
										<li class="nav-item">
											<a class="nav-link <?= ($y == 0) ? 'active' : ''; ?>" id="home-<?= $y; ?>-tab" data-toggle="tab"
											   href="#home-<?= $y; ?>" role="tab" aria-controls="home-<?= $y; ?>" aria-selected="true">
												<?= $item['pro_tab']; ?>
											</a>
										</li>
									<?php endforeach; ?>
								</ul>
								<div class="tab-content" id="myTabContent">
									<?php foreach ($fields['project_tabs'] as $x => $item) : ?>
										<div class="tab-pane fade <?= ($x == 0) ? 'show active' : ''; ?>" id="home-<?= $x; ?>" role="tabpanel"
											 aria-labelledby="home-<?= $x; ?>-tab">
											<div class="base-output tab-pro-output">
												<?= $item['text']; ?>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					<?php endif; ?>
					<div class="<?= $fields['project_tabs'] ? 'col-xl-4 col-lg-5 col-12' : 'col-12'; ?>">
						<div class="dark-back form-col-post pro-col-post">
							<?php if ($f_title && $f_subtitle) : ?>
								<div class="d-flex flex-column align-items-center mb-3">
									<h2 class="contact-form-title"><?= $f_title; ?></h2>
									<h2 class="contact-form-subtitle"><?= $f_subtitle; ?></h2>
								</div>
							<?php endif;
							getForm('15'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php if ($fields['project_gallery']) : ?>
	<section class="block-gallery">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<h2 class="base-title same-title-underline text-center mb-5">
						<?= esc_html__('גלריה', 'leos'); ?>
					</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="tabs-content-gallery">
						<ul class="nav nav-tabs" id="myTab" role="tablist">
							<?php foreach ($fields['project_gallery'] as $y => $item) : ?>
								<li class="nav-item">
									<a class="nav-link nav-link-gallery <?= ($y == 0) ? 'active' : ''; ?>" id="gallery-<?= $y; ?>-tab" data-toggle="tab"
									   href="#gallery-<?= $y; ?>" role="tab" aria-controls="gallery-<?= $y; ?>" aria-selected="true">
										<?= $item['gal_title']; ?>
									</a>
								</li>
							<?php endforeach; ?>
						</ul>
						<div class="tab-content" id="myTabContent">
							<?php foreach ($fields['project_gallery'] as $x => $item) : ?>
								<div class="tab-pane fade <?= ($x == 0) ? 'show active' : ''; ?>" id="gallery-<?= $x; ?>" role="tabpanel"
									 aria-labelledby="gallery-<?= $x; ?>-tab">
									<?php if (isset($item['gallery'])) : $chunks = array_chunk($item['gallery'], 6);
										$quantity = count($chunks);
									foreach ($chunks as $m => $chunk) {
										if ($m === 0) {
											get_template_part('views/partials/card', 'gallery', [
												'gallery' => $chunk,
												'num' => $m,
											]);
										}
									} endif;
									if ($quantity && $quantity > 1) : ?>
										<div class="row justify-content-center mt-4">
											<div class="col-auto">
												<div class="more-link base-link load-more-posts more-link-cards" data-type="gallery" data-count="<?= $quantity; ?>" data-page="<?= get_queried_object_id(); ?>" data-num="<?= $x; ?>">
													<?= esc_html__('טען עוד..', 'leos'); ?>
												</div>
											</div>
										</div>
									<?php endif; ?>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'project_cat', ['fields' => 'ids']);
$samePosts = [];
$samePosts = (isset($fields['same_posts']) && $fields['same_posts']) ? $fields['same_posts'] : '';
$samePosts = get_posts([
	'posts_per_page' => 6,
	'post_type' => 'project',
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => 'project_cat',
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($samePosts === NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 6,
		'orderby' => 'rand',
		'post_type' => 'project',
		'post__not_in' => array($postId),
	]);
}
if ($samePosts) : ?>
<section class="posts-pro-block">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto d-flex justify-content-center">
				<h2 class="base-title same-title-underline text-center mb-5">
					<?= (isset($fields['same_title']) && $fields['same_title']) ? $fields['same_title'] : esc_html__('השקעות נוספות', 'leos');?>
				</h2>
			</div>
		</div>
		<div class="row justify-content-center align-items-stretch arrows-slider">
			<div class="col-12">
				<div class="posts-slider" dir="rtl">
					<?php foreach ($samePosts as $post) : ?>
						<div>
							<?php get_template_part('views/partials/card', 'post',
								[
									'post' => $post,
								]); ?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif;
get_footer(); ?>
