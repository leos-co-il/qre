<?php

add_action('wp_ajax_nopriv_ajax_function', 'ajax_function');
add_action('wp_ajax_more_ajax_function', 'ajax_function');

function ajax_function()
{

    if (!wp_verify_nonce($_REQUEST['nonce'], "my_user_vote_nonce")) {
        exit("No naughty business please");
    }

    $result['type'] = "error";
    $result['var'] = 1;

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $result = json_encode($result);
        echo $result;
    } else {
        header("Location: " . $_SERVER["HTTP_REFERER"]);
    }
    die();
}
add_action('wp_ajax_nopriv_get_more_function', 'get_more_function');
add_action('wp_ajax_get_more_function', 'get_more_function');

function get_more_function() {
	$ids_string = (isset($_REQUEST['ids'])) ? $_REQUEST['ids'] : '';
	$id_term = (isset($_REQUEST['termID'])) ? $_REQUEST['termID'] : '';
	$id_page = (isset($_REQUEST['page'])) ? $_REQUEST['page'] : '';
	$number = (isset($_REQUEST['number'])) ? $_REQUEST['number'] : '';
	$count = (isset($_REQUEST['countAll'])) ? $_REQUEST['countAll'] : '';
	$quantityNow = (isset($_REQUEST['quantity'])) ? $_REQUEST['quantity'] : '';
	$type = (isset($_REQUEST['postType'])) ? $_REQUEST['postType'] : 'post';
	$termName = (isset($_REQUEST['termName'])) ? $_REQUEST['termName'] : 'category';
	$ids = explode(',', $ids_string);
	$query = new WP_Query([
		'post_type' => $type,
		'posts_per_page' => 3,
		'post__not_in' => $ids,
		'suppress_filters' => false,
		'tax_query' => $id_term && $termName? [
			[
				'taxonomy' => $termName,
				'field' => 'term_id',
				'terms' => $id_term,
			]
		] : '',
	]);
	$html = '';
	$result['html'] = '';
	if ($query->have_posts() && ($type !== 'project')) {
		foreach ($query->posts as $item) {
			$html = load_template_part('views/partials/card', 'post', [
				'post' => $item,
			]);
			$result['html'] .= $html;
			if ($count <= ($quantityNow + 3)) {
				$result['quantity'] = true;
			}
		}
	} elseif ($query->have_posts() && ($type === 'project')) {
		foreach ($query->posts as $item) {
			$html = load_template_part('views/partials/card', 'project', [
				'post' => $item,
			]);
			$result['html'] .= $html;
			if ($count <= ($quantityNow + 3)) {
				$result['quantity'] = true;
			}
		}
	}  elseif (($type === 'gallery') && $id_page) {
		$query = get_field('project_gallery', $id_page);
		$x = 0;
		foreach ($query[$number] as $i => $item) {
			$item_new = array_chunk($item, 6);
			if (!in_array($i, $ids)) {
				$html = load_template_part('views/partials/card', 'gallery', [
					'gallery' => $item,
					'num' => $i,
				]);
				$result['html'] .= $html;
				$x++;
			}
			if ($x >= 6) {
				break;
			}
		}
		if (count($query) <= ($quantityNow + 1)) {
			$result['quantity'] = true;
		}
	}

	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();
}
