<?php

function text_preview($phrase, $max_words) {
    $phrase = strip_shortcodes($phrase);
    $phrase = strip_tags($phrase);
    $phrase_array = explode(' ',$phrase);
    if(count($phrase_array) > $max_words && $max_words > 0)
        $phrase = implode(' ',array_slice($phrase_array, 0, $max_words)).'...';
    return $phrase;
}

function postThumb($post = false, $size = 'full', $placeholder_size = 'regular', $placeholder = true){
    $postThumb = IMG . 'placeholder-'. $placeholder_size .'.png';
    if(ENV === 'dev' || ENV === 'qa'){
        $postThumb = 'https://picsum.photos/60' . mt_rand(1,9);
    }
    if(!$post){
        $post = get_post();
    }
    if($post && get_the_post_thumbnail_url($post)){
        $postThumb = get_the_post_thumbnail_url($post, $size);
    }

    if($placeholder){
        return $postThumb;
    }else{
        return false;
    }

}
function getYoutubeId($url){
    parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
    return $my_array_of_vars['v'];
}
function getYoutubeThumb($url){
    return "https://img.youtube.com/vi/".getYoutubeId($url)."/0.jpg";
}
function getForm($id){
    echo do_shortcode('[contact-form-7 id="'.$id.'"  html_class="use-floating-validation-tip"]');
}
function svg_simple($path){
    return file_get_contents($path);
}
function svg_ext($url, $class='',$field = '',$alt = '') {
    $nosvg =  ICONS . 'nosvg.png';
    if ($field == 'field') {
        $url = get_field($url);
    } elseif ($field == 'options') {
        $url = get_field($url,'options');
    }
    if (is_array($url)) {
        if (empty($alt)) { $alt = $url[alt]; }
        $url = $url[url];
        $svg = $url;
    } else {
        $svg = $url;
    }
    $svg = explode('.', $svg);
    $csvg = count($svg);
    if ($csvg == 1) {
        $path = TEMPLATEPATH.'/images/'.$svg[0].'.svg';
        if (file_exists($path)) {
            $svg = file_get_contents($path);
            $svg = explode('<svg',$svg);
            $svg = '<svg class="'.$class.' anim" '.$svg[1];
        } else {
            $svg = '<img src="'.$nosvg.'" alt="" class="'.$class.'"/>';
        }
    } else {
        $place = $csvg - 1;
        if ($svg[$place] == 'svg') {
            $url = explode('/',$url);
            $c = count($url) - 1;
            $path = WP_CONTENT_DIR.'/uploads/'.$url[$c - 2].'/'.$url[$c - 1].'/'.$url[$c];
            $svg = file_get_contents($path);
            $svg = explode('<svg',$svg);
            $svg = '<svg class="'.$class.' anim" '.$svg[1];
        } else {
            $svg = '<img src="'.$url.'" alt="'.$alt.'" class="'.$class.'"/>';
        }
    }
    return $svg;
}
function getMenu($location, $depth, $menu_class = '', $container_class = ''){
    wp_nav_menu([
        'theme_location' => $location,
        'container'       => 'div',
        'container_id'    => '',
        'container_class' => $container_class,
        'menu_id'         => false,
        'menu_class'      => $menu_class,
        'depth'           => $depth,
    ]);
}
function opt($field_name){
    if(function_exists('get_field')){
        return get_field($field_name, 'options');
    }
    return null;
}
function getPhone(){
    $ph = opt('tel');
    $phone = [];
    if ($ph) {
        $phone["txt"] = $ph;
        $phone["num"] = preg_replace("/[^A-Za-z0-9]/", "",$ph);
        $phone["tel"] = 'tel:'.$phone["num"];
        $phone["link"] = '<a href="tel:'.$phone["num"].'">'.$ph.'</a>';
    }

    return $phone;
}

function getAddress(){
    $address = opt('address');
    if ($address) {
        $addr = explode(',',$address["address"]);
        $address["name"] = $addr[0].', '.$addr[1];
        $address["gmlink"] = 'http://www.google.com/maps/place/'.$address["lat"].','.$address["lng"];
        $address["waze"] = 'waze://?q='.$address["name"];
    }

    return $address;
}
function include_part($name, $once = false){
    locate_template('partials/'.$name.'.php', true, $once);
}
function getPageByTemplate($template){
	$args = [
		'post_type' => 'page',
		'fields' => 'ids',
		'nopaging' => true,
		'meta_key' => '_wp_page_template',
		'meta_value' => $template
	];
	$pages = get_posts( $args );
	return $pages[0];
}
function load_template_part($template_name, $part_name = null, $params) {
	ob_start();
	get_template_part($template_name, $part_name, $params);
	$var = ob_get_contents();
	ob_end_clean();
	return $var;
}
function get_lang() {
	if ( function_exists('icl_object_id') ) {
		return ICL_LANGUAGE_CODE;
	}
	return null;
}
function site_languages() {
	if ( function_exists( 'icl_get_languages' ) ) :
		$languages = icl_get_languages( 'skip_missing=N&orderby=KEY&order=DIR&link_empty_to=str' );
		if ( ! empty( $languages ) ) :
			echo "\n<ul class=\"languages\">\n";
			foreach ( $languages as $lang ) :
				echo '<li class="' . ( $lang['active'] ? 'active' : ''  ) . '"><a href=' . $lang['url'] . '>' . $lang['language_code'] . "</a></li>\n";
			endforeach;
			echo "</ul>\n";
		endif; // ( ! empty( $languages ) )
	endif; // ( function exists )
}
function site_breadcrumbs() {

	/* === Options === */
	$text['home']     = esc_html__('דף הבית ', 'leos');
	$text['category'] = '%s';
	$text['search']   = esc_html__('תוצאות חיפוש ', 'leos').'"%s"';
	$text['tag']      = esc_html__('פוסטים עם תג ', 'leos').'"%s"';
	$text['author']   = esc_html__('פוסטים של מחבר ', 'leos').'%s';
	$text['404']      = esc_html__('שגיאה 404 - העמוד אינו נמצא ', 'leos');
	$text['page']     = '%s';
	$text['cpage']    = esc_html__('עמוד עם הערות עבור ', 'leos').'"%s"';

	$wrap_before    = '<ul class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">';
	$wrap_after     = '</ul><!-- .breadcrumbs -->';
	$sep            = '&nbsp;'.'>'.'&nbsp;';
	$before         = '<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><span itemprop="name">';
	$after          = '</span><meta itemprop="position" content="2" /></li>';

	$show_on_home   = 0;
	$show_home_link = 1;
	$show_current   = 1;
	$show_last_sep  = 1;
	/* === End of the options === */

	global $post;
	$home_url       = home_url('/');
	$link           = '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
	$link          .= '<a class="breadcrumbs__link" href="%1$s" itemprop="item"><span itemprop="name">%2$s</span></a>';
	$link          .= '<meta itemprop="position" content="%3$s" />';
	$link          .= '</li>';
	$parent_id      = ( $post ) ? $post->post_parent : '';
	$home_link      = sprintf( $link, $home_url, $text['home'], 1 );

	if ( is_home() || is_front_page() ) {

		if ( $show_on_home ) echo $wrap_before . $home_link . $wrap_after;

	} else {

		$position = 0;

		echo $wrap_before;

		if ( $show_home_link ) {
			$position += 1;
			echo $home_link;
		}

		if ( is_category() ) {
			$parents = get_ancestors( get_query_var('cat'), 'category' );
			foreach ( array_reverse( $parents ) as $cat ) {
				$position += 1;
				if ( $position > 1 ) echo $sep;
				echo sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
			}
			if ( get_query_var( 'paged' ) ) {
				$position += 1;
				$cat = get_query_var('cat');
				echo $sep . sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
				echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
			} else {
				if ( $show_current ) {
					if ( $position >= 1 ) echo $sep;
					echo $before . sprintf( $text['category'], single_cat_title( '', false ) ) . $after;
				} elseif ( $show_last_sep ) echo $sep;
			}

		} elseif ( is_search() ) {
			if ( get_query_var( 'paged' ) ) {
				$position += 1;
				if ( $show_home_link ) echo $sep;
				echo sprintf( $link, $home_url . '?s=' . get_search_query(), sprintf( $text['search'], get_search_query() ), $position );
				echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
			} else {
				if ( $show_current ) {
					if ( $position >= 1 ) echo $sep;
					echo $before . sprintf( $text['search'], get_search_query() ) . $after;
				} elseif ( $show_last_sep ) echo $sep;
			}

		} elseif ( is_year() ) {
			if ( $show_home_link && $show_current ) echo $sep;
			if ( $show_current ) echo $before . get_the_time('Y') . $after;
			elseif ( $show_home_link && $show_last_sep ) echo $sep;

		} elseif ( is_month() ) {
			if ( $show_home_link ) echo $sep;
			$position += 1;
			echo sprintf( $link, get_year_link( get_the_time('Y') ), get_the_time('Y'), $position );
			if ( $show_current ) echo $sep . $before . get_the_time('F') . $after;
			elseif ( $show_last_sep ) echo $sep;

		} elseif ( is_day() ) {
			if ( $show_home_link ) echo $sep;
			$position += 1;
			echo sprintf( $link, get_year_link( get_the_time('Y') ), get_the_time('Y'), $position ) . $sep;
			$position += 1;
			echo sprintf( $link, get_month_link( get_the_time('Y'), get_the_time('m') ), get_the_time('F'), $position );
			if ( $show_current ) echo $sep . $before . get_the_time('d') . $after;
			elseif ( $show_last_sep ) echo $sep;

		} elseif ( is_single() && ! is_attachment() ) {
			if ( get_post_type() != 'post' ) {
				$position += 1;
				$post_type = get_post_type_object( get_post_type() );
				if ( $position > 1 ) echo $sep;
				echo sprintf( $link, get_post_type_archive_link( $post_type->name ), $post_type->labels->name, $position );
				if ( $show_current ) echo $sep . $before . get_the_title() . $after;
				elseif ( $show_last_sep ) echo $sep;
			} else {
				$cat = get_the_category(); $catID = $cat[0]->cat_ID;
				$parents = get_ancestors( $catID, 'category' );
				$parents = array_reverse( $parents );
				$parents[] = $catID;
				foreach ( $parents as $cat ) {
					$position += 1;
					if ( $position > 1 ) echo $sep;
					echo sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
				}
				if ( get_query_var( 'cpage' ) ) {
					$position += 1;
					echo $sep . sprintf( $link, get_permalink(), get_the_title(), $position );
					echo $sep . $before . sprintf( $text['cpage'], get_query_var( 'cpage' ) ) . $after;
				} else {
					if ( $show_current ) echo $sep . $before . get_the_title() . $after;
					elseif ( $show_last_sep ) echo $sep;
				}
			}

		} elseif ( is_post_type_archive() ) {
			$post_type = get_post_type_object( get_post_type() );
			if ( get_query_var( 'paged' ) ) {
				$position += 1;
				if ( $position > 1 ) echo $sep;
				echo sprintf( $link, get_post_type_archive_link( $post_type->name ), $post_type->label, $position );
				echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
			} else {
				if ( $show_home_link && $show_current ) echo $sep;
				if ( $show_current ) echo $before . $post_type->label . $after;
				elseif ( $show_home_link && $show_last_sep ) echo $sep;
			}

		} elseif ( is_attachment() ) {
			$parent = get_post( $parent_id );
			$cat = get_the_category( $parent->ID ); $catID = $cat[0]->cat_ID;
			$parents = get_ancestors( $catID, 'category' );
			$parents = array_reverse( $parents );
			$parents[] = $catID;
			foreach ( $parents as $cat ) {
				$position += 1;
				if ( $position > 1 ) echo $sep;
				echo sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
			}
			$position += 1;
			echo $sep . sprintf( $link, get_permalink( $parent ), $parent->post_title, $position );
			if ( $show_current ) echo $sep . $before . get_the_title() . $after;
			elseif ( $show_last_sep ) echo $sep;

		} elseif ( is_page() && ! $parent_id ) {
			if ( $show_home_link && $show_current ) echo $sep;
			if ( $show_current ) echo $before . get_the_title() . $after;
			elseif ( $show_home_link && $show_last_sep ) echo $sep;

		} elseif ( is_page() && $parent_id ) {
			$parents = get_post_ancestors( get_the_ID() );
			foreach ( array_reverse( $parents ) as $pageID ) {
				$position += 1;
				if ( $position > 1 ) echo $sep;
				echo sprintf( $link, get_page_link( $pageID ), get_the_title( $pageID ), $position );
			}
			if ( $show_current ) echo $sep . $before . get_the_title() . $after;
			elseif ( $show_last_sep ) echo $sep;

		} elseif ( is_tag() ) {
			if ( get_query_var( 'paged' ) ) {
				$position += 1;
				$tagID = get_query_var( 'tag_id' );
				echo $sep . sprintf( $link, get_tag_link( $tagID ), single_tag_title( '', false ), $position );
				echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
			} else {
				if ( $show_home_link && $show_current ) echo $sep;
				if ( $show_current ) echo $before . sprintf( $text['tag'], single_tag_title( '', false ) ) . $after;
				elseif ( $show_home_link && $show_last_sep ) echo $sep;
			}

		} elseif ( is_author() ) {
			$author = get_userdata( get_query_var( 'author' ) );
			if ( get_query_var( 'paged' ) ) {
				$position += 1;
				echo $sep . sprintf( $link, get_author_posts_url( $author->ID ), sprintf( $text['author'], $author->display_name ), $position );
				echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
			} else {
				if ( $show_home_link && $show_current ) echo $sep;
				if ( $show_current ) echo $before . sprintf( $text['author'], $author->display_name ) . $after;
				elseif ( $show_home_link && $show_last_sep ) echo $sep;
			}

		} elseif ( is_404() ) {
			if ( $show_home_link && $show_current ) echo $sep;
			if ( $show_current ) echo $before . $text['404'] . $after;
			elseif ( $show_last_sep ) echo $sep;

		} elseif ( has_post_format() && ! is_singular() ) {
			if ( $show_home_link && $show_current ) echo $sep;
			echo get_post_format_string( get_post_format() );
		}

		echo $wrap_after;

	}
} // end of site_breadcrumbs()
