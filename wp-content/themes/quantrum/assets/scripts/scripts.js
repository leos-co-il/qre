(function($) {
	$.fn.slideFadeToggle  = function(speed, easing, callback) {
		return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
	};
	$('.pop-great').click(function() {
		$('.float-form').removeClass('show-float-form');
		$('.pop-great').removeClass('show-pop');
	});
	$('.pop-great').click(function(event){
		event.stopPropagation();
	});
	$( document ).ready(function() {
		$('.pop-trigger').click(function () {
			$('.pop-great').toggleClass('show-pop');
			$('.float-form').toggleClass('show-float-form');
		});
		var menu = $( '.drop-menu' );
		$('.hamburger').click(function () {
			menu.slideFadeToggle();
			$(this).toggleClass('is-active');
		});
		$( '.rev-pop-trigger' ).click( function() {
			var contentAll = $( this ).children('.hidden-review').html();
			$( '#reviews-pop-wrapper' ).html( contentAll );
			$( '#reviewsModal' ).modal( 'show' );
		});
		$( '#reviewsModal' ).on( 'hidden.bs.modal', function( e ) {
			$( '#reviews-pop-wrapper' ).html( '' );
		});
		$('.cat-item-img').hover(function (){
			$(this).children('.cat-item-info').children('.cat-item-info-part').slideFadeToggle();
		});
		$('.search-trigger').click(function () {
			$('.pop-search').addClass('show-search');
			$('.float-search').addClass('show-float-search');
		});
		$('.close-search').click(function () {
			$('.pop-search').removeClass('show-search');
			$('.float-search').removeClass('show-float-search');
		});
		$('.pop-trigger').click(function () {
			$('.pop-form').addClass('show-popup');
			$('.float-form').addClass('show-float-form');
			$('.pop-body').addClass('curr-body-hidden');
		});
		$('.close-form').click(function () {
			$('.pop-form').removeClass('show-popup');
			$('.float-form').removeClass('show-float-form');
			$('.pop-body').removeClass('curr-body-hidden');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$('.main-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: false,
			dots: true,
		});
		$('.posts-slider').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			rtl: true,
			dots: false,
			arrows: true,
			responsive: [
				{
					breakpoint: 1050,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 576,
					settings: {
						slidesToShow: 2,
					}
				},
			]
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		$('.play-button').click(function() {
			var id = $(this).data('video');
			var frame = '<iframe width="100%" height="500px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('#iframe-wrapper').html(frame);
			$('#modalCenter').modal('show');
		});
		$('#modalCenter').on('hidden.bs.modal', function (e) {
			$('#iframe-wrapper').html('');
		});
		$('.play-button-post').click(function() {
			var id = $(this).data('id');
			var iFrame = '<iframe src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('.put-video-here').addClass('show').html(iFrame);
		});
		var accordion = $('#accordion');
		accordion.on('shown.bs.collapse', function () {
			var show = $( '.show' );
			show.parent().children('.question-title').children('.faq-icon').addClass('active-icon');
			show.parent().children('.question-title').children('.faq-minus').addClass('show-sign');
		});
		accordion.on('hidden.bs.collapse', function () {
			var collapsed = $( '.collapse' );
			collapsed.parent().children('.question-title').children('.faq-minus').removeClass('show-sign');
			collapsed.parent().children('.question-title').children('.faq-icon').removeClass('active-icon');
		});
		//Socials
		$('.trigger-wrap').hover(function(){
			$(this).children('.all-socials').addClass('show-share');
		}, function(){
			$(this).children('.all-socials').removeClass('show-share');
		});
	});
	$('.load-more-posts').click(function(e) {
		e.preventDefault();
		var btn = $(this);
		btn.addClass('loading');
		btn.append('<div class="cart-loading"><i class="fas fa-spinner fa-pulse"></i></div>');
		var postType = $(this).data('type');
		var termID = $(this).data('term');
		var termName = $(this).data('term_name');
		// var params = $('.take-json').html();
		var ids = '';
		var page = $(this).data('page');
		var number = $(this).data('num');
		var countAll = $(this).data('count');
		var quantity = $('.more-card').length;
		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				postType: postType,
				termID: termID,
				number: number,
				termName: termName,
				ids: ids,
				page: page,
				quantity: quantity,
				countAll: countAll,
				// taxType: taxType,
				action: 'get_more_function',
			},
			success: function (data) {
				btn.removeClass('loading');
				$('.cart-loading').remove();
				if (!data.html || data.quantity) {
					btn.addClass('hide');
				}
				$('.put-here-posts').append(data.html);
			}
		});
	});
})( jQuery );
