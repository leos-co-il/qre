<?php
get_header();
$query = get_queried_object();
$posts = new WP_Query([
	'posts_per_page' => 6,
	'post_type' => 'project',
	'suppress_filters' => false,
	'tax_query' => [
		[
			'taxonomy' => 'project_cat',
			'field' => 'term_id',
			'terms' => $query->term_id,
		]
	]
]);
$published_posts = new WP_Query([
	'posts_per_page' => -1,
	'post_type' => 'project',
	'suppress_filters' => false,
	'tax_query' => [
		[
			'taxonomy' => 'project_cat',
			'field' => 'term_id',
			'terms' => $query->term_id,
		]
	]
]);
$img = get_field('top_img', $query);
?>
<article class="page-body blog-body">
	<?php get_template_part('views/partials/repeat', 'top_block', [
		'title' => $query->name,
		'img' => $img ? $img : '',
	]); ?>
	<div class="container my-5">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="base-output text-center mb-4">
					<?= category_description(); ?>
				</div>
			</div>
		</div>
		<?php if ($posts->have_posts()) : ?>
			<div class="row align-items-stretch put-here-posts justify-content-center">
				<?php foreach ($posts->posts as $post) {
					get_template_part('views/partials/card', 'post',
						[
							'post' => $post,
						]);
				} ?>
			</div>
		<?php endif;
		if ($published_posts->have_posts() && (($num = count($published_posts->posts)) > 6)) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="more-link regular-link load-more-posts" data-type="project"
						 data-count="<?= $num; ?>" data-term="<?= $query->term_id; ?>" data-term_name="project_cat">
						<?= esc_html__('טען עוד..', 'leos'); ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php
get_template_part('views/partials/repeat', 'form');
if ($seo = get_field('single_slider_seo', $query)) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $seo,
		'img' => get_field('slider_img', $query),
	]);
}
if ($faq = get_field('faq_item', $query)) {
	get_template_part('views/partials/content', 'faq',
		[
			'block_title' => get_field('faq_title', $query),
			'faq' => $faq,
		]);
}
get_footer(); ?>

